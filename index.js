const express = require("express");
const path = "./public/uploads";
const fs = require("fs");
const multer = require("multer");
const upload = multer({ dest: path });
const port = 3000;
const app = express();

app.use(express.static(path));


app.post("/upload", upload.single("myfile"), (req, res) => {

    fs.readdir(path, function (err, items) {
        console.log("uploaded" + req.file.filename)
        res.send(
            `<h1> File Upload Successful </h1>
      <a href="/"><button>Back</button></a> <br />
      <img src=${req.file.filename} height=300px/>`
        )

    })
})



app.get("/", function (req, res) {
    let photos = ''
    fs.readdir(path, function (err, items) {
        console.log(items);
        for (let i = 0; i < items.length; i++) {
            photos += `<img src=${items[i]} height=150px><br />`;
        }
        res.send(
            ` <h1>Welcome to Kenziegram</h1>
<form id="user-image-upload" action="/upload" enctype="multipart/form-data" method="post">
<input type="file" name="myfile" id="upload"/>
<input type="submit"/>
</form><br />${photos}`)
    })
})

app.listen(port, () =>
    console.log("Server running on http://localhost:" + port)
);
